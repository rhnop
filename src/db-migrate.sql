alter table master drop column ismusic, add column carttype ENUM('show','pool','jingle') default 'pool';
alter table master modify column carttype ENUM('show','pool','jingle');
alter table standby drop column ismusic, add column carttype ENUM('show','pool','jingle') default 'pool';
alter table standby modify column carttype ENUM('show','pool','jingle');
