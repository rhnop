--
--  rhnop
--
--  Copyright (C) 2011-2016 Christian Pointner <equinox@helsinki.at>
--
--  This file is part of rhnop.
--
--  rhnop is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  any later version.
--
--  rhnop is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with rhnop. If not, see <http://www.gnu.org/licenses/>.
--

local last_carts = nil

posix = require "posix"
mq = require "mq"

package.path = package.path .. ";" .. rhnoplibdir .. "/?.lua"
playlog = require "playlog"
rddb = require "rddb"
conf = require "conf"


local cnf = {}
function init(conffile)
   cnf = conf.load(conffile)
end

function handle_now(timestamp, output, nowcart, nowlen)
   local results, err = rddb:getCartInfo(nowcart)
   if results == nil then
      io.stderr:write("ERROR: can't fetch cart info: " .. err .. "\n")
      return true
   else
      local showtitle, carttype = rddb:getCartShowName(nowcart)
      if showtitle == nil then
         io.stderr:write("ERROR: can't fetch show/pool name: " .. carttype .. "\n")
         return true
      else
         local ret, err = playlog:insertNow(timestamp, nowcart, nowlen, showtitle, results.TITLE, results.ARTIST, results.ALBUM, carttype, output)
         if ret == nil then
            io.stderr:write("ERROR: can't insert cart info: " .. err .. "\n")
         else
            pipe.signal(timestamp)
         end
      end
   end

   return true
end

function handle_message(msg, q)
   local timestamp, output, nowcart, nowlen, nextcart, nextlen = string.match(msg, "^(%d+) (%d+) (%d+) (%d+) (%d+) (%d+)$");
   if not timestamp or not output or not nowcart or not nowlen or not nextcart or not nextlen then
      io.stderr:write("WARN: ignoring malformed message\n")
   else
      nowcart = tonumber(nowcart)
      nowlen = tonumber(nowlen)
      if last_carts[output] == nil then last_carts[output] = 0 end
      if last_carts[output] ~= nowcart and nowlen > 0 then
         last_carts[output] = nowcart
         local ret = handle_now(timestamp, output, nowcart, nowlen)
         if ret == nil then
            io.stderr:write("INFO: trying to push last message back onto the queue - before exiting\n")
            local result, err = mq.send(q, timestamp .. " " .. output .. " " .. nowcart .. " " .. nowlen .. " " .. nextcart .. " " .. nextlen, 0)
            if result == nil then
               io.stderr:write("ERROR: sending message failed: " .. err .. "\n")
            end
            return nil
         end
      end
   end

   return true
end

function main_loop()
   init(rhnopescdir .. "/nopfetchd.conf")

   posix.umask("rwxrwxr-x")
   local q, err = mq.create(cnf.queue_name, "rw", "rw-rw----")
   if q == nil then
      q, err = mq.open(cnf.queue_name, "wo")
      if q == nil then
         io.stderr:write("ERROR: creation of message queue failed: " .. err .. "\n")
         os.exit(1)
      end
   end

   local ret, err = playlog:init(cnf)
   if ret == nil then
      io.stderr:write("ERROR: creation of playlog failed: " .. err .. "\n")
      os.exit(1)
   end
   last_carts = assert(playlog:getLastCarts())
   print("PLAYLOG: connected to " .. cnf.playlog_db .. "@" .. cnf.playlog_host .. " with user '" .. cnf.playlog_user .. "'")

   local ret, err = rddb:init(cnf)
   if ret == nil then
      io.stderr:write("ERROR: opening rivendell db failed: " .. err .. "\n")
      playlog:close()
      os.exit(1)
   end
   print("RDDB: connected to " .. cnf.rddb_db .. "@" .. cnf.rddb_host .. " with user '" .. cnf.rddb_user .. "'")

   while true do
      local msg, prio = mq.receive(q)
      if msg == nil then
         io.stderr:write("ERROR mq.receive(): " .. prio .. "\n")
         rddb:close()
         playlog:close()
         os.exit(2)
      end
      local ret = handle_message(msg, q)
      if ret == nil then
         os.exit(1)
      end
   end
end
