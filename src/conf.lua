--
--  rhnop
--
--  Copyright (C) 2011-2016 Christian Pointner <equinox@helsinki.at>
--
--  This file is part of rhnop.
--
--  rhnop is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  any later version.
--
--  rhnop is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with rhnop. If not, see <http://www.gnu.org/licenses/>.
--

local conf = {}

function conf.load(conffile)
   local cnf = {}

   local file = assert(io.open(conffile, "r"))
   for line in file:lines() do
      local k,v = string.match(line, "^([^=#]+)=(.*)$")
      if k and v and v ~= "" then
         cnf[k] = v
      end
   end
   file:close()

   return cnf
end

return conf
