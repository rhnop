/*
 *  rhnop
 *
 *  Copyright (C) 2011-2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhnop.
 *
 *  rhnop is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhnop is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhnop. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOPFETCHD_l_pipe_h_INCLUDED
#define NOPFETCHD_l_pipe_h_INCLUDED

#include <lua.h>

int pipe_init();
void pipe_close();

#define LUA_PIPELIBNAME "pipe"
LUALIB_API int luaopen_pipe(lua_State *L);

#endif
