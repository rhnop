--
--  rhnop
--
--  Copyright (C) 2011-2016 Christian Pointner <equinox@helsinki.at>
--
--  This file is part of rhnop.
--
--  rhnop is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  any later version.
--
--  rhnop is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with rhnop. If not, see <http://www.gnu.org/licenses/>.
--

luasql = require "luasql.mysql"

local rddb = {}

function rddb:init(cnf)
   local err

   self.env, err = luasql.mysql()
   if self.env == nil then
      return nil, err
   end

   self.con, err = self.env:connect(cnf.rddb_db, cnf.rddb_user, cnf.rddb_pwd, cnf.rddb_host, cnf.rddb_port)
   if self.con == nil then
      return nil, err
   end

   local ret, err = self.con:execute("SET CHARACTER SET utf8")
   if ret == nil then
      return nil, err
   end

   return true
end

function rddb:getCartInfo(cartnum)
   local cur, err = self.con:execute("select TITLE,ARTIST,ALBUM from CART where NUMBER = " .. self.con:escape(cartnum))
   if cur == nil then
      return nil, err
   end

   if cur:numrows() ~= 1 then
      cur:close()
      return nil, "nothing found in rivendell db"
   end

   local results = {}
   results, err = cur:fetch(results, "a")
   cur:close()

   if results.TITLE == nil then results.TITLE = "" end
   if results.ARTIST == nil then results.ARTIST = "" end
   if results.ALBUM == nil then results.ALBUM = "" end

   return results, err
end

function rddb:getGroupNameAndCartType(cartnum)
   local cur, err = self.con:execute("select NAME,DESCRIPTION from GROUPS where DEFAULT_LOW_CART <= " .. self.con:escape(cartnum) .. " and DEFAULT_HIGH_CART >= " .. self.con:escape(cartnum))
   if cur == nil then
      return nil, err
   end

   local groupname = nil
   local carttype = nil
   local groupdesc = nil
   while true do
      local data = {}
      data = cur:fetch(data, "a")
      if data == nil then break end
      if data.NAME == "ALL_SHOWS" then
         carttype = "show"
      elseif data.NAME == "ALL_POOLS" then
         carttype = "pool"
      elseif data.NAME == "ALL_JINGLE" then
         carttype = "jingle"
      else
         groupname = data.NAME
         groupdesc = data.DESCRIPTION
      end
      if groupname and carttype then break end
   end
   cur:close()

   if carttype == nil then
      return nil, "unable to detect group type for cart: " .. cartnum
   end
   if groupname == nil then
      return nil, "can't find any group for cart: " .. cartnum
   end
   return carttype, groupname, groupdesc
end

local getShowType = function(params)
   local n = 0
   for p in string.gmatch(params, "([^;]+)") do
      if n == 5 then
         return p
      end
      n = n + 1
   end
   return "n"
end

local getShowLog = function(macros)
   local log = string.match(macros, "^LL 1 ([^ ]+) 0!$")
   if log == nil then
      return nil, "invalid show macro"
   end
   log = string.gsub(log, "-", "_") .. "_LOG"
   if string.match(log, "^[_0-9a-zA-Z-]+$") == nil then
      return nil, "invalid show log-name: " .. log
   end
   return log
end

function rddb:__showHasCart(showlog, cartnum)
   local cur, err = self.con:execute("select CART_NUMBER from " .. showlog .. " where CART_NUMBER = " .. tonumber(cartnum))
   if cur == nil then
      return nil, err
   end
   local nresults = cur:numrows()
   cur:close()

   return nresults > 0
end

function rddb:__getShowNameForShowCart(cartnum, groupname)
   local cur, err = self.con:execute("select CART.TITLE,CART.MACROS,DROPBOXES.SET_USER_DEFINED as PARAMS from CART,DROPBOXES where CART.NUMBER = DROPBOXES.TO_CART and DROPBOXES.GROUP_NAME = '" .. self.con:escape(groupname) .. "'")
   if cur == nil then
      return nil, err
   end

   while true do
      local data = {}
      data = cur:fetch(data, "a")
      if data == nil then break end

      local showtype = getShowType(data.PARAMS)
      if showtype == "r" then
         data.TITLE = data.TITLE .. " (Wiederholung)"
      elseif showtype == "s" then
         data.TITLE = data.TITLE .. " (Sondersendung)"
      end

      local log = getShowLog(data.MACROS)

      if self:__showHasCart(log, cartnum) then
         cur:close()
         return data.TITLE, "show"
      end
   end
   cur:close()

   return nil, "can't find any show for cart: " .. cartnum
end

function rddb:getCartShowName(cartnum)
   local carttype, groupname, groupdesc = self:getGroupNameAndCartType(cartnum)
   if carttype == nil then
      return nil, groupname
   end
   if carttype == "show" then
      return rddb:__getShowNameForShowCart(cartnum, groupname)
   end
   return groupdesc, carttype
end

function rddb:close()
   if self.con then
      self.con:close()
   end

   if self.env then
      self.env:close()
   end
end

return rddb
